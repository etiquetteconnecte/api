Il faut tout d'abord installer PHP 7.4 minimum.

Puis il faut installer un sgbd type mysql.

Lancer le SQBG et PHP.

Créer une base de données nommer : klub

Metter vous sur le dossier du projet.
Lancer les commandes suivante pour les migrations :
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load

Installer open ssl sur le lien suivant : 
https://slproweb.com/download/Win64OpenSSL-1_1_1i.msi

Generer les clés SSL avec la passphrase se trouvant dans le fichier .env :
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout