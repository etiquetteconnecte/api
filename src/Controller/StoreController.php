<?php
declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/store")
 */
class StoreController extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @Route("/read/{storeId}", name="store_read", methods={"GET"})
     * @param $storeId
     * @return JsonResponse
     */
    public function read($storeId): JsonResponse
    {
        $store = $this->em->getRepository('App:Store')
            ->find($storeId);

        return $this->json([
            'id' => $store->getId(),
        ]);
    }
}
