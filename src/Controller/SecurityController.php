<?php
declare(strict_types=1);

namespace App\Controller;

use _HumbugBoxd11d6e1365a3\Nette\Neon\Exception;
use App\Entity\Store;
use App\Entity\StoreManager;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use App\Entity\User;

/**
 * @Route("/api")
 */
class SecurityController extends AbstractController
{
    /**
     * @var JWTEncoderInterface
     */
    private JWTEncoderInterface $encoderInterface;
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(
        EntityManagerInterface $em,
        JWTEncoderInterface $encoderInterface,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->em = $em;
        $this->encoderInterface = $encoderInterface;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/login_check", name="login_check", methods={"POST"})
     */
    public function login(): JsonResponse
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    }

    /**
     * @Route("/register", name="api_register",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function register(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $validator = Validation::createValidator();
        $constraint = new Assert\Collection(array(
            // the keys correspond to the keys in the input array
            'password' => new Assert\Length(array('min' => 1)),
            'email' => new Assert\Email(),
            'role' => new Assert\Optional()
        ));
        $violations = $validator->validate($data, $constraint);
        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string)$violations], 500);
        }

        $roles = ['ROLE_USER'];
        $password = $data['password'];
        $email = $data['email'];
        $role = $data['role'];

        $userExists = $this->em->getRepository('App:User')
            ->findOneBy(['email' => $email]);

        if ($userExists) {
            return new JsonResponse(["error" => "Cet email est déjà pris"], 500);
        }

        $user = new User();

        if ($role) {
            $roles[] = $role;
            if ($role == 'ROLE_MANAGER') {
                $user = new StoreManager();
                $store = new Store();
                $user->setStore($store);
                $this->em->persist($store);
            }
        }

        $user
            ->setEmail($email)
            ->setPassword($this->passwordEncoder->encodePassword($user, $password))
            ->setRoles($roles)
        ;

        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }
        return new JsonResponse(["success" => $user->getUsername(). " has been registered!"], 200);
    }
}
