<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('admin_klub@yopmail.com');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'azerty123'
        ));
        $manager->persist($admin);

        $storeManager = new User();
        $storeManager->setEmail('manager_klub@yopmail.com');
        $storeManager->setRoles(['ROLE_MANAGER']);
        $storeManager->setPassword($this->passwordEncoder->encodePassword(
            $storeManager,
            'azerty123'
        ));
        $manager->persist($storeManager);

        $manager->flush();
    }
}
